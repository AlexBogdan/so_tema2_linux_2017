/**
 * Operating Systems 2013-2017 - Assignment 2
 *
 * TODO Name, Group
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <fcntl.h>
#include <unistd.h>

#include "cmd.h"
#include "utils.h"

#define READ		0
#define WRITE		1

static int set_var(const char *var, const char *value)
{
	/* TODO - Set the environment variable */
	setenv(var, value, 1);
	return 0;
}

static char *expand(const char *key)
{
	/* TODO - Return the value of environment variable */
	return getenv(key);
}

static void expand_word(word_t *word)
{
	word_t *part;
	char result[1024] = "";

	if (word == NULL)
		return;

	for (part = word; part != NULL; part = part->next_part) {
		if (part->expand == true) {
			part->string = expand(part->string);
			if (part->string == NULL)
				part->string = strdup("");
		}
		sprintf(result, "%s%s", result, part->string);
	}
	word->string = strdup(result);
}

static void expand_command(simple_command_t *s)
{
	word_t *param, *part;

	expand_word(s->verb);
	for (param = s->params; param != NULL; param = param->next_word)
		expand_word(param);
	expand_word(s->in);
	expand_word(s->out);
	expand_word(s->err);
}

/**
 * Internal change-directory command.
 */
static int shell_cd(const simple_command_t *s)
{
	int fd, ret;
	char *filename = NULL;

	if (s->out != NULL)
		filename = strdup(s->out->string);
	if (s->err != NULL)
		filename = strdup(s->err->string);

	if (filename != NULL) {
		if (s->io_flags == IO_REGULAR)
			fd = open(filename, O_WRONLY|O_CREAT|O_TRUNC, 0644);
		else if (s->io_flags == IO_OUT_APPEND ||
				 s->io_flags == IO_ERR_APPEND)
			fd = open(filename, O_WRONLY|O_CREAT|O_APPEND, 0644);
		DIE(fd < 0, "[SHELL_CD] Nu am putut deschide fisierul!");
	}

	if (s->params == NULL)
		return -1;

	ret = chdir(s->params->string);
	if (ret == -1)
		fprintf(stderr, "Unable to find file or directory!\n");


	return ret;
}

/**
 * Internal exit/quit command.
 */
static int shell_exit(void)
{
	/* TODO execute exit/quit */
	fclose(stdin);
	fclose(stdout);
	fclose(stderr);
	return SHELL_EXIT; /* TODO replace with actual exit code */
}

/**
 *	Vom redirecta filedes primit catre un fisier nou in functie de
 * tipul redirectarii (normal sau append) si de filedes (stdin/out/err)
 */
static void redirect(int filedes, const char *filename, int io_flag)
{
	int ret;
	int fd;

	switch (filedes) {
	case STDIN_FILENO:
		fd = open(filename, O_RDONLY, 0644);
		break;
	case STDOUT_FILENO:
	case STDERR_FILENO:
		if (io_flag == IO_REGULAR)
			fd = open(filename, O_WRONLY|O_CREAT|O_TRUNC, 0644);
		else if (io_flag == IO_OUT_APPEND ||
				 io_flag == IO_ERR_APPEND)
			fd = open(filename, O_WRONLY|O_CREAT|O_APPEND, 0644);
		break;
	default:
		return;
	}
	DIE(fd < 0, "open");

	ret = dup2(fd, filedes);
	DIE(ret < 0, "dup2");

	close(fd);
}

/**
 * Verificam ce redirectari avem de facut pentru comanda curenta
 */
static void set_redirect_files(simple_command_t *s)
{
	int ret;

	switch (s->io_flags) {
	case IO_REGULAR:
		/* Redirectam stdin*/
		if (s->in != NULL)
			redirect(STDIN_FILENO, s->in->string, s->io_flags);

		/*	Daca avem cmd &> file vom seta
		 * stdout si stderr pe acelasi fd
		 */
		if (s->out != NULL && s->err != NULL) {
			redirect(STDOUT_FILENO, s->out->string, s->io_flags);
			// printf("%s\n", s->err->string);
			if (strcmp(s->out->string, s->err->string) == 0) {
				ret = dup2(STDOUT_FILENO, STDERR_FILENO);
				DIE(ret < 0, "dup2");
			} else {
				redirect(STDERR_FILENO, s->err->string,
						 s->io_flags);
			}
			break;
		}
		/* Redirectam stdout normal */
		if (s->out != NULL)
			redirect(STDOUT_FILENO, s->out->string, s->io_flags);
		/* Redirectam stderr normal */
		if (s->err != NULL)
			redirect(STDERR_FILENO, s->err->string, s->io_flags);
		break;
	case IO_OUT_APPEND:
		/* Redirectam stdout append */
		redirect(STDOUT_FILENO, s->out->string, s->io_flags);
		break;
	case IO_ERR_APPEND:
		/* Redirectam stderr append */
		redirect(STDERR_FILENO, s->err->string, s->io_flags);
		break;
	}
}

/**
 * Parse a simple command (internal, environment variable assignment,
 * external command).
 */
static bool parse_simple(simple_command_t *s, int level, command_t *father)
{
	/* TODO sanity checks */

	/* TODO if builtin command, execute the command */

	pid_t child_pid, wait_ret;
	int status;

	char **parameters = NULL;
	word_t *param = NULL;
	int no_params = 0, i;

	// printf("Expand : %s\n", expand(s->params->string));

	expand_command(s);

	/* Cream un nou proces care sa execute comanda */
	child_pid = fork();
	switch (child_pid) {
	case -1:	/* error */
		perror("fork");
		exit(EXIT_FAILURE);

	case 0:		/* child process */
		set_redirect_files(s);

		/* Numaram cati parametri avem in comanda */
		no_params = 0;
		for (param = s->params; param != NULL; param = param->next_word)
			++no_params;

		/* Alocam spatiu pentru cmd + no_params + 1 NULL final*/
		parameters = (char **) malloc(sizeof(char *) * (no_params + 2));

		/*	Copiem cmd pe prima pozitie
		 * dupa cele N argumente
		 * si terminam cu NUL
		 */
		parameters[0] = strdup(s->verb->string);
		i = 1;
		for (param = s->params; param != NULL;
				param = param->next_word) {
			parameters[i] = strdup(param->string);
			++i;
		}
		parameters[i] = NULL;

		/* Executam comanda */
		execvp(parameters[0], (char *const *) parameters);

		fprintf(stderr, "Execution failed for '%s'\n", parameters[0]);
		fflush(stdout);

		exit(EXIT_FAILURE);

	default:	/* parent process */
		wait_ret = waitpid(child_pid, &status, 0);
		DIE(wait_ret < 0, "waitpid");
	}

	if (parameters != NULL) {
		for (i = 0; i < no_params+1; ++i)
			free(parameters[i]);
		free(parameters);
	}

	/* TODO if variable assignment, execute the assignment and return
	 * the exit status
	 */

	/* TODO if external command:
	 *   1. fork new process
	 *     2c. perform redirections in child
	 *     3c. load executable in child
	 *   2. wait for child
	 *   3. return exit status
	 */

	if (WIFEXITED(status))
		return WEXITSTATUS(status);
	else
		return EXIT_FAILURE;
}

/**
 * Process two commands in parallel, by creating two children.
 */
static bool do_in_parallel(command_t *cmd1, command_t *cmd2, int level,
		command_t *father)
{
	/* TODO execute cmd1 and cmd2 simultaneously */
	int ret, status1, status2;
	pid_t pid1, pid2, wait_ret;

	pid1 = fork();
	switch (pid1) {
	case -1:
		perror("[PIPE] fork child1");
		exit(EXIT_FAILURE);
		break;
	case 0: /* child1 */
		ret = parse_command(cmd1, level, father);
		exit(ret);
		break;
	default:  /* parrent */
		pid2 = fork();
		switch (pid2) {
		case -1:
			perror("[PIPE] fork child1");
			exit(EXIT_FAILURE);
		case 0: /* child2 */
			ret = parse_command(cmd2, level, father);
			exit(ret);
			break;
		default:  /* parrent */
			break;
		}

		wait_ret = waitpid(pid1, &status1, 0);
		DIE(wait_ret < 0, "[PIPE] procesul 2 nu a esuat");

		wait_ret = waitpid(pid2, &status2, 0);
		DIE(wait_ret < 0, "[PIPE] procesul 2 nu a esuat");
	}

	if (WIFEXITED(status1) && WIFEXITED(status2))
		return WEXITSTATUS(status1) | WIFEXITED(status2);
	else
		return EXIT_FAILURE;
}

static void close_filedes(int *filedes) {
	int ret;

	ret = close(filedes[READ]);
	DIE(ret < 0, "nu am inchis filedes[0]");

	ret = close(filedes[WRITE]);
	DIE(ret < 0, "nu am putut inchide filedes[1]");
}

/**
 * Run commands by creating an anonymous pipe (cmd1 | cmd2)
 */
static bool do_on_pipe(command_t *cmd1, command_t *cmd2, int level,
		command_t *father)
{
	/* TODO redirect the output of cmd1 to the input of cmd2 */
	int filedes[2], ret, status, result;
	pid_t pid1, pid2, wait_ret;

	/* Cream un pipe */
	ret = pipe(filedes);
	DIE(ret < 0, "[PIPE] eroare create pipe");

	/* Pornim primul proces */
	pid1 = fork();
	switch (pid1) {
	case -1:
		perror("[PIPE] fork child1");
		exit(EXIT_FAILURE);
	case 0: /* child1 */
		pid2 = fork();
		switch (pid2) {
		case -1:
			perror("[PIPE] fork child2");
			exit(EXIT_FAILURE);
		case 0: /* child2 */
			ret = dup2(filedes[WRITE], STDOUT_FILENO);
			DIE(ret < 0, "[PIPE] nu am putut redirecta `in`");

			close_filedes(filedes);

			result = parse_command(cmd1, level, father);
			DIE(result < 0, "[PIPE] comanda a esuat");

			exit(result);
		default: /* child1 */
			ret = dup2(filedes[READ], STDIN_FILENO);
			DIE(ret < 0, "[PIPE] nu am putut redirecta `in`");

			close_filedes(filedes);

			result = parse_command(cmd2, level, father);
			DIE(result < 0, "[PIPE] comanda a esuat");

			exit(result);
		}
	default: /* parrent */
		close_filedes(filedes);

		wait_ret = waitpid(pid1, &status, 0);
		DIE(wait_ret < 0, "[PIPE] procesul 2 nu a esuat");
	}

	if (WIFEXITED(status))
		return WEXITSTATUS(status);
	else
		return EXIT_FAILURE;
}

/**
 * Parse and execute a command.
 */
int parse_command(command_t *c, int level, command_t *father)
{
	/* TODO sanity checks */
	int ret, ret1, ret2;
	char *var;

	if (c->op == OP_NONE) {
		var = strdup(c->scmd->verb->string);

		/* Verificam daca avem true / false */
		if (strncmp("true", var, strlen("true")) == 0)
			return false;
		if (strncmp("false", var, strlen("false")) == 0)
			return true;

		/* Testam cele 3 comenzi interne */
		if (strncmp("exit", var, strlen("exit")) == 0)
			return shell_exit();
		if (strncmp("quit", var, strlen("quit")) == 0)
			return shell_exit();
		if (strncmp("cd", var, strlen("cd")) == 0)
			return shell_cd(c->scmd);

		/* Verificam daca avem variabila de mediu */
		if (c->scmd->verb != NULL &&
			c->scmd->verb->next_part != NULL &&
			strcmp("=", c->scmd->verb->next_part->string) == 0 &&
			c->scmd->verb->next_part->next_part != NULL) {

			expand_command(c->scmd);
			var = strdup(c->scmd->verb->string);

			char *token = strtok(var, "=");

			token = strtok(NULL, "");
			return set_var(var, token);
		}

		return parse_simple(c->scmd, level+1, father);
	}

	switch (c->op) {
	case OP_SEQUENTIAL:
		/* TODO execute the commands one after the other */
		ret1 = parse_command(c->cmd1, level+1, father);
		ret2 = parse_command(c->cmd2, level+1, father);
		ret = ret1 & ret2;
		break;

	case OP_PARALLEL:
		/* TODO execute the commands simultaneously */
		ret = do_in_parallel(c->cmd1, c->cmd2, level+1, father);
		break;

	case OP_CONDITIONAL_NZERO:
		/* TODO execute the second command only if the first one
		 * returns non zero
		 */
		ret1 = parse_command(c->cmd1, level+1, father);
		if (ret1 != 0)
			ret2 = parse_command(c->cmd2, level+1, father);
		ret = ret1 & ret2;
		break;

	case OP_CONDITIONAL_ZERO:
		/* TODO execute the second command only if the first one
		 * returns zero
		 */
		ret1 = parse_command(c->cmd1, level+1, father);
		if (ret1 == 0)
			ret2 = parse_command(c->cmd2, level+1, father);
		ret = ret1 | ret2;
		break;

	case OP_PIPE:
		/* TODO redirect the output of the first command to the
		 * input of the second
		 */
		ret = do_on_pipe(c->cmd1, c->cmd2, level, father);
		break;

	default:
		return SHELL_EXIT;
	}

	return ret; /* TODO replace with actual exit code of command */
}
